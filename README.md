# CRYPTENBIN

## Description
blah blah

## Installation
```
$ apt-get install python3-pip python3.4-venv supervisor
$ git clone ...
$ ...
$ pyvenv-3.4 venv3
$ source venv3/bin/activate
(venv3)$ pip3 install tornado ujson tinydb
(venv3)$ python3 app.py
```

or if you prefer python2.7
```
$ apt-get install python-pip -y && git clone ... && virtualenv venv && source venv/bin/activate && pip install tornado ujson tinydb 
$ python app.py
```


##  NOTES

### Deploy
```
$ apt-get install nginx, 
$ ...
...
$ sudo adduser crypt
$ chown crypt:crypt .
$ sudo su crypt
crypt$
crypt$ source ./venv3/bin/activate
crypt$ pip install -r requirements.txt
crypt$ exit
$ cat <<EOF > crypten.conf 
[program:crypten]
user=crypt
directory=/home/crypt/cryptenbin/
environment=PATH="/home/crypt/cryptenbin/venv3/bin"
command=/home/crypt/cryptenbin/venv3/bin/python3 /home/crypt/cryptenbin/app.py
stdout_logfile=/tmp/mytornado.log
stderr_logfile=/tmp/mytornado.error.log
EOF
$ 
$ systemctl status supervisor.service ### prefer systemctl from service
$ systemctl status supervisor.service
$ supervisorctl update
$ supervisorctl status
```

### fun with asmcrypt.js
```
$ git clone https://github.com/vibornoff/asmcrypto.js.git
$ cd asmcrypto.js
$ vi Gruntfile.js # add 'sha512' in the 'defaults' list
$ npm install     # or npm install --unsafe-perm if you r running root 
```

