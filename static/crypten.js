// Additionall stuff
function isint(v){ return typeof v==="number" && isFinite(v) && Math.floor(v) === v; };
function randomString(length,chars) {
    // var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var chars = (typeof chars != "undefined") && (typeof chars != null) ? 
        chars+"0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz" :
        "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var randomstring = '';
    for (var i=0; i<length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum,rnum+1);
    }
    return randomstring;
}

//
//
//  CRYPTEN OBJECT 
//
//
/*
echo <base64 result> | openssl enc -d -aes-256-cbc -md sha256 -base64 -salt -pass pass:secret
****************************************************************** 
      Implement Openssl AES-CBC using SHA512 / SHA256 in javascript using asmCrypto.js
      based on http://joelinoff.com/blog/?p=885
  ******************************************************************* 
*/

function Crypten(){
    this.name = 'Crypten object';
    this.klen = 32;
    this.ilen = 16;
    this.crypto = window.asmCrypto;
    this.sha = 'SHA256'; // (this.crypto.SHA512) ? 'SHA512' : 'SHA256';
    
};

Crypten.prototype.getBytes = function(s){
    // check if is string then convert else return 
    s =  (s instanceof Uint8Array) ? s : (typeof s === 'string') ? this.crypto.string_to_bytes(s) : null;
    if(s!==null) return s;
    else throw 'Unknown type, expected String or Uint8Array';
};

Crypten.prototype.getString = function(s){
    // check if is string return else convert or throw 
    s =  (typeof s === 'string') ? s : (s instanceof Uint8Array) ? this.crypto.bytes_to_string(s) : null;
    if(s!==null) return s;
    else throw 'Unknown type, expected String or Uint8Array';
};

Crypten.prototype.appendUint8Array = function(a, b){
    a = this.getBytes(a);
    b = this.getBytes(b);
    var r = new Uint8Array( a.length + b.length );
    r.set( a, 0 );
    r.set( b, a.length );
    return r;
};

Crypten.prototype.salt = function(len){
    len =  isint(len) ? len : 8;
    return this.crypto.getRandomValues(new Uint8Array(len));
};

Crypten.prototype.hash = function(s){
    s = this.getBytes(s);
    return ( this.sha === 'SHA512' ) ? this.crypto.SHA512.bytes(s) : this.crypto.SHA256.bytes(s);
};

Crypten.prototype.pad = function(p){
    /* TODO : Replace String.fromCharCode(p) with actual ord function
     * http://phpjs.org/functions/ord/
     **/
    if( (isint(p) ? p : null) === null ) throw 'Uknown type, expected intreger'; 
    var a='';
    for(var i=0;i<p;i++) { a+=String.fromCharCode(p); }
    return a;
};

Crypten.prototype.padded = function(plain){
    plain = this.getString(plain);
    padding_len = 16 - (plain.length % 16)
    padding = this.pad(padding_len);
    padded_plain = plain + padding;
    return this.crypto.string_to_bytes(padded_plain);
};

Crypten.prototype.unpadded = function(paddedtext){
    /* TODO : Replace .charCodeAt() with following actual chr function
     * http://phpjs.org/functions/chr/
     * */
    paddedtext = this.getString(paddedtext);
    var padding_len = paddedtext.charCodeAt(paddedtext.length-1);
    return paddedtext.substring(0, paddedtext.length-padding_len);
};

Crypten.prototype.get_key_and_iv = function(password, salt){
    password = this.getBytes(password);
    var key=null,iv=null;
    var keyiv = this.hash( this.appendUint8Array(password,salt) );
    var tmp = [keyiv];
    while (tmp.length< this.klen + this.ilen){
        var _t = this.appendUint8Array(
                    this.appendUint8Array(tmp[tmp.length-1],password),
                    salt);
        tmp.push( this.hash(_t) );
        keyiv = this.appendUint8Array(keyiv,tmp[tmp.length-1]);
        key = keyiv.slice(0,this.klen);
        iv = keyiv.slice(this.klen,this.klen+this.ilen);
    }
    return [key, iv];
};

Crypten.prototype.isBase64 = function(s) {
    try {
        return btoa(atob(s)) == s;
    } catch (err) {
        return false;
    }
}

Crypten.prototype.chunkit = function(s){
    /* TODO : validate base64 */
    if (!(typeof s === 'string')) throw 'Unknown type, expected string!';
    if (!this.isBase64(s)) throw 'Expected base64 encoded string!';
    var r = '', linelen = 64;
    for( var i=0; i<s.length; i+=linelen){
        r += s.substring(i, Math.min(i+linelen, s.length) ) + "\n";
    }
    return r;
};

Crypten.prototype.openssl_encrypt = function(password, plain){
    password  = this.getBytes(password);
    plain = this.getString(plain);
    var salt = this.salt();
    // [key,iv] = this.get_key_and_iv(password, salt);
    var keyiv = this.get_key_and_iv(password, salt);
    var key=keyiv[0]; 
    var iv=keyiv[1];
    var paddedtext = this.padded(plain);
    var enc = this.crypto.AES_CBC.encrypt(paddedtext, key, false, iv);
    var openssl_ciphertext = this.appendUint8Array(
        this.appendUint8Array( this.crypto.string_to_bytes('Salted__'),salt),
        enc);
    var chunk = this.chunkit( this.crypto.bytes_to_base64(openssl_ciphertext) );
    return chunk; 
};

Crypten.prototype.filter = function(text){
    
    return this.getString(text);
    
    /* 
     * NOT IN USE
     *
     * filter -- ignore blank lines and comments
     * */
    text = this.getString(text);
    var filtered = '';
    text.split("\n").forEach(function(line){
        line = line.replace(/^\s+|\s+$/g, '');
        if ( !line || line.search('^\s*$') || line.search('^\s*#')) return;
        filtered += line + '\n';
    });
    return filtered;
};

Crypten.prototype.openssl_decrypt = function(password, encrypted){
    /*
     * TODO : check if 'encrypted' is base64 before procceed  
     * */
    password  = this.getBytes(password);
    /* patch for line breaks -- seems we do not need this */
    var filtered = this.filter(encrypted);
    var raw = this.crypto.base64_to_bytes(filtered);
    if ('Salted__' !== this.crypto.bytes_to_string(raw.slice(0,8)))
        throw "Not recognised encrypted format, expected openssl compatible enrypted content";
    var salt = raw.slice(8,16);
    var ciphertext = raw.slice(16,raw.length);

    // [key,iv] = this.get_key_and_iv(password, salt);
    var keyiv = this.get_key_and_iv(password, salt);
    var key=keyiv[0]; 
    var iv=keyiv[1];
    var dec = this.crypto.AES_CBC.decrypt(ciphertext, key, false, iv);
    var unpadded = this.unpadded(dec);
    var unpadded = (unpadded.charCodeAt(unpadded.length-1)===10)? unpadded.substring(0,unpadded.length-1):unpadded;
    return unpadded;
};
