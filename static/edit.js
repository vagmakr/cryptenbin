$(document ).ready(function(){(function defer(){if (window.jQuery && window.asmCrypto && window.Crypten){
    var $nm = (function() {
        var nm = {
            uname:'',
            edit_secret:'',
            alert:null,
            post:null,
            c:null,
        };
        return nm;
    });
    $nm.c = new Crypten();
    $nm.alert = function(e){alert(e);}
    $nm.post = function post(url, data, callback){
        $.ajax({
            url: url,
            type: 'POST',
            contentType:'application/json',
            data: JSON.stringify(data),
            dataType:'json',
            success: function(data){
                callback(data);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $nm.alert('! There was a connection problem while trying to store the file');
            }
        });
    }

    $("#decrypt").click(function(){
        if ($("#passwd").val() == 'password' ) {
            $nm.alert('Please provide a password to decrypt the message!');
        }
        else{
            var content = $nm.c.openssl_decrypt($("#passwd").val(),$("#content").val());
            if( content == '' ){
                $nm.alert('Incorrect password!');
            }
            else{
                $('#content').val(content);
                $("#save").show();
            }
        }
    });

    $("#save").click(function(){
        var encrypted = $nm.c.openssl_encrypt( $("#passwd").val(), $("#content").val());
        encrypted = encrypted.replace(/(\r\n|\n|\r)/gm,""); // remove breaklines
        var data = {'content': encrypted,'name':$nm.uname, 'edit_secret':$nm.edit_secret };
        $nm.post(
            location.origin+'/save/'+$nm.uname,
            data,
            function(data){
                if (data.status !== 'success') {
                    if(data.status === 'error') $nm.alert(data.msg);
                    else $nm.alert('Sorry there was an error!');
                }
            }
        );
        $('#content').val(encrypted);
    });

    var path = location.pathname;
    $nm.edit_secret = location.hash.substring(1, location.hash.length);
    $nm.uname = path.substring(path.indexOf('/edit/') + '/edit/'.length, path.length );
    $nm.post(
        location.origin+'/raw/'+$nm.uname,
        {name:$nm.uname},
        function(data){ // Response
            if (data.status === 'success')
                $('#content').val(data.content);
            else if(data.status === 'error')
               $nm.alert(data.msg);
            else
               $nm.alert('Sorry there was an error!');
        });

    console.log( "Ready to roll!" );
    $("#save").hide();
}else{ setTimeout(function(){defer()},50);}})();});
