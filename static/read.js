$(document ).ready(function(){(function defer(){if (window.jQuery && window.asmCrypto && window.Crypten){
    var $nm = (function() {
        var nm = {
            encrypted:false,
            alert: null,
            post:null,
            c:null,
        };
        return nm;
    });
    $nm.c = new Crypten();
    $nm.alert = function(e){alert(e);}
    $nm.post = function post(url, data, callback){
        $.ajax({
            url: url,
            type: 'POST',
            contentType:'application/json',
            data: JSON.stringify(data),
            dataType:'json',
            success: function(data){
                callback(data);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $nm.alert('! There was a connection problem while trying to store the file');
            }
        });
    }

    $("#decrypt").click(function(){
        if ($("#passwd").val() == 'password' ) {
            $nm.alert('Please provide a password to decrypt the message!');
        }
        else{
            var content = $nm.c.openssl_decrypt($("#passwd").val(),$("#content").val());
            if( content == '' ){
                $nm.alert('Incorrect password!');
            }
            else{
                $('#content').val(content);            
            }
        }
    });

    var path = location.pathname;
    var edit_secret = location.hash.substring(1, location.hash.length);
    var name = path.substring(path.indexOf('/read/') + '/read/'.length, path.length );
    var data = {
        edit_secret:edit_secret,
        name:name
    }

    $nm.post(location.origin+'/raw/'+name, data,
        function(data){ // Response
            if (data.status === 'success')
                $('#content').val(data.content);
            else if(data.status === 'error')
               $nm.alert(data.msg);
            else
               $nm.alert('Sorry there was an error!');
        });

    console.log( "Ready to roll!" );
}else{ setTimeout(function(){defer()},50);}})();});
