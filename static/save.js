$(document ).ready(function(){(function defer(){if (window.jQuery && window.asmCrypto && window.Crypten){
    var $nm = (function() {
        var nm = {
            encrypted:false,
            alert: null,
            post:null,
            c:null,
        };
        return nm;
    });
    $nm.c = new Crypten();
    $nm.alert = function(e){alert(e);}
    $nm.post = function post(url, data, callback){
        $.ajax({
            url: url,
            type: 'POST',
            contentType:'application/json',
            data: JSON.stringify(data),
            dataType:'json',
            success: function(data){
                callback(data);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $nm.alert('! There was a connection problem while trying to store the file');
            }
        });
    }

    function create_links(parent_id, this_id, text, link) {
        if ( window.location.port === 80 ) { link = window.location.protocol + "//" + window.location.hostname + link; }
        else                               { link = window.location.protocol + "//" + window.location.host + link; }
        if ($('#'+this_id).parent().length) $('#'+this_id).remove(); // Remove if exists
        $('#'+parent_id).append( '<a id="'+this_id+'" href="'+link+'">'+link+'</a>'); // create link
        return $('#'+this_id); // return link element
    }

    function show_urls(data){
        create_links("read_url", "read_url_link", data.read_url, data.read_url).show();
        create_links("write_url", "write_url_link", data.write_url, data.write_url).show();
        $("#result").show("slow");
        $("#read_url").show("slow");
        $("#write_url").show("slow");
    }

    $("#encrypt").click(function(){
        if ($("#passwd").val() == 'password' ) {
            $nm.alert('Please provide a password or click generate to generate one!');
        }
        else{
            var content = $("#content").val();
            var encrypted = $nm.c.openssl_encrypt( $("#passwd").val(), content);
            $('#content').val( encrypted );
            $nm.encrypted = true;
        }
    });

    $("#save").click(function(){
        $("#result").hide();
        /*  if ($nm.fileloaded) { // FOR BROWSE FILE -- not implemented yet
            $('#content').val($nm.c.openssl_encrypt($("#passwd").val(),$nm.c.crypto.string_to_bytes($nm.content)););
            $nm.fileloaded=false; } */
        if ( $nm.encrypted ) {
            var encrypted = $("#content").val();
            encrypted = encrypted.replace(/(\r\n|\n|\r)/gm,""); // remove breaklines
            var exp_code = parseInt($("#expires").val());
            var data = {'content': encrypted,'expiration_code': exp_code};
            $nm.post(
                '/save', // url
                data,   // request data
                function(data){ // responce data
                    data.read_url = data.read_url ? data.read_url : ''
                    data.write_url = data.write_url ? data.write_url : ''
                    show_urls(data);
                }
            );
        }
        else{
            $nm.alert('Please Encrypt the file first!');
        }
    });

    $(".result").hide();
    $('#content').val('Write something here');


    /* -----------------------------------------------------------------------------------------
    *   ADD browse button
    *   Decryption of wild characters is not working properly
    * -----------------------------------------------------------------------------------------
    function createDownloadLink(el,filename, text) {
        // Remove if exists
        if ($("#download").parent().length)
            $("#download").remove();
        // Create link
        $(el).append( '<a id="download">Download</a>');
        $("#download").attr({
            'href':'data:text/plain;charset=utf-8,' + encodeURIComponent(text),
            'download' : filename,
        });
        return $("#download");
    }
    $("#browse").change(function(e){
        if(window.File && window.FileReader && window.FileList && window.Blob){
            if(e.target.files.length==1){
                var reader = new FileReader();
                reader.onload = function(e){
                    // *  Filetypes
                    // * data:application/pdf;base64
                    // * data:application/octet-stream;base64
                    // * data:text/plain;base64
                    // * data:application/x-perl;base64 ... python
                    // * data:image/jpeg;base64
                    // * data:application/zip;base64
                    // * data:application/gzip;base64
                    // * data:application/x-7z-compressed;base64
                    // * data:application/x-php;base64
                    // * data:video/mp4;base64
                    // * data:application/x-iwork-keynote-sffkey;base64
                    var type = ( e.target.result.split(',').length > 1 ) ?  e.target.result.split(',')[0] :  e.target.result;
                    $nm.alert(type);
                    var b64 = ( e.target.result.split(',').length > 1 ) ?  e.target.result.split(',')[1] :  e.target.result;
                    $nm.content = atob(b64);
                };

                // Check Filetype
                var checksize = (e.target.files[0].size > 1024*1024) ? confirm('File too big, might crash your browser! Continue?'): true;
                if(checksize){
                    reader.readAsDataURL(e.target.files[0]); // returns dataurl / base64
                    $nm.fileloaded=true;
                }
                else{
                    $('#browse').val("");
                    $nm.fileloaded=false;
                }
            }
        } else $nm.alert('The File APIs are not fully supported in this browser.');
    });*/
    console.log( "Ready to roll!" );
}else{ setTimeout(function(){defer()},50);}})();});
