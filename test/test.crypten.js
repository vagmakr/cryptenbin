var system = require('system'),argv;
var fs = require('fs');

if(system.args.length<=3){
 console.log('Usage: '+system.args[0]+'<e/d encrypt or decrypt> <password> <some file>');
 slimer.exit();
}else{
  var e = system.args[1];
  var content = fs.read(system.args[3]); //console.log(content);
  var password = system.args[2];
  var page = require("webpage").create();
  var url="http://127.0.0.1:8888";
  
  page.onLoadFinished=function(status){if(status !== "success") console.log("Server is down. Please run server");
    var args = [e, password, content];
    var res = page.evaluate(function(args) {
      var c = new Crypten();
      if (args[0]=='e') return c.openssl_encrypt(args[1], args[2]);
      else if (args[0]=='d') return c.openssl_decrypt(args[1], args[2]);
      else return 'wrong arguments!';
    }, args);
    console.log(res);
    page.close();
    slimer.exit();
  };
  page.open(url);
}
