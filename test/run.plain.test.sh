#!/bin/bash
#
#	run tests with slimer 
#
# ------------------------------------
# openssl compatible commands :
# echo <base64 encrypted>      | openssl enc -d -aes-256-cbc -md sha256 -base64 -salt -pass pass:secret
# echo <plain text to encrypt> | openssl enc -e -aes-256-cbc -md sha256 -base64 -salt -pass pass:secret
# ------------------------------------
server="http://127.0.0.1:8888"
slimer="`pwd`/slimer.sh"

if [[ "$#" == "2" ]]
  then
    passwd="$1"
    plain="$2"
  else
    echo 'Please provide password and some text to test';
    echo "   e.g.: $0 '<password>' '<plain text>'";
    exit
fi
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
LRED='\033[1;31m'
LBLUE='\033[1;34m'
LGREEN='\033[1;32m'
WHITE='\033[1;37m'
NC='\033[0m'

#
# Given Argument
#
# printf "Using : ${BLUE}$passwd $plain${NC}\n"

#
# Encrypt it ($plain)
#
enc=$(echo $plain | openssl enc -e -aes-256-cbc -md sha256 -base64 -salt -pass pass:"$passwd" | tr '\n' ' ')
enc=$(echo $enc |sed 's/ //g')
printf "Encrypted using openssl: ${RED}$enc${NC}\n"

#
# Create test case for encrypted argument ($a)
#
cat <<EOF > test.decrypt.js
function p(_p){console.log(_p);};
function close(){ if(page){page.close();} slimer.exit();}
var page = require("webpage").create();
var url="$server";
function run(){
    var c = page.evaluate(function(){ var c = new Crypten(); return c.openssl_decrypt('${passwd}','${enc}'); });
    p(c);
    close();
}
page.onLoadFinished = function (status){
    if (status == "success")    run();
    else    console.log("Server not found! Please run the server first!");
};
page.open(url);
EOF

cat <<EOF > test.encrypt.js
function p(_p){console.log(_p);};
function close(){ if(page){page.close();} slimer.exit();}
var page = require("webpage").create();
var url="$server";
function run(){
    var c = page.evaluate(function(){ var c = new Crypten(); return c.openssl_encrypt('${passwd}','${plain}'); });
    p(c);
    close();
}
page.onLoadFinished = function (status){
    if (status == "success")    run();
    else    console.log("Server not found! Please run the server first!");
};
page.open(url);
EOF

jdec="$($slimer test.decrypt.js)"
printf "Decrypted using javascript: ${GREEN}$jdec${NC}\n"

jenc="$($slimer test.encrypt.js)" 
printf "Encrypted using javascript: ${GREEN}$jenc${NC}\n"

odec=$(echo "$jenc" | openssl enc -d -aes-256-cbc -md sha256 -base64 -salt -pass pass:"$passwd")
printf "Decrypted using openssl: ${RED}$odec${NC}\n"

if [[ "$plain" == "$jdec" ]];
  then
    printf "${WHITE}Decryption Test${NC}:${LGREEN}PASSED!${NC}\n"
  else
    printf "${WHITE}Decryption Test${NC}:${LRED}FAILED!${NC}\n"
fi

if [[ "$plain" == "$odec" ]];
  then
    printf "${WHITE}Enryption Test${NC}:${LGREEN}PASSED!${NC}\n"
  else
    printf "${WHITE}Encryption Test${NC}:${LRED}FAILED!${NC}\n"
fi

kill -9 $xpid &> /dev/null
wait $PID 2>/dev/null

rm test.encrypt.js test.decrypt.js
