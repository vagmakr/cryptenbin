# run tests
The folder contains 2 simple tests regarding the encryption and decryption proccess between cryptenbin and openssl. 

### Okay, first install `slimerjs-0.9.6`
https://docs.slimerjs.org/current/installation.html

### Second edit `slimer.sh` and 
1. replace slimer path
2. replace server (default is 127.0.0.1:8888)

### Then:
run server in a seperate terminal 
```
$ cd ../ && source venv3/bin/activate && python3 app.py
```

and execute tests
```
./run.file.test.sh <password> <plain text>
./run.plain.test.sh <password> <file>
```


I run the tests under debian jessie, hope they work for you :)
 