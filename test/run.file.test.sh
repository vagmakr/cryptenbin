#!/bin/bash
#
#	run tests with slimer 
#
# ------------------------------------
# openssl compatible commands :
# echo <base64 encrypted>      | openssl enc -d -aes-256-cbc -md sha256 -base64 -salt -pass pass:secret
# echo <plain text to encrypt> | openssl enc -e -aes-256-cbc -md sha256 -base64 -salt -pass pass:secret
# ------------------------------------
slimer="`pwd`/slimer.sh"
script="`pwd`/test.crypten.js"
server="http://127.0.0.1:8888"

if [[ "$#" == "2" ]]
  then
    passwd="$1"
    file="$2"
  else
    echo 'Please provide password and file to test';
    echo "   e.g.: $0 '<password>' '<path to file>'";
    exit
fi

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
LRED='\033[1;31m'
LBLUE='\033[1;34m'
LGREEN='\033[1;32m'
WHITE='\033[1;37m'
NC='\033[0m'



#
#	test 1 : from crypten to openessl
#
printf "${GREEN}[+]${NC} Encrypt using crypten.js\n"

$slimer $script e $passwd $file > enc.out

printf "${BLUE}[+]${NC} Decrypt using openssl\n"

openssl enc -d -in enc.out -aes-256-cbc -md sha256 -base64 -salt -pass pass:$passwd > dec.out

res=$(diff dec.out $file)

if [[ -z "$res" ]];
  then
    printf "${WHITE}Test${NC}:${LGREEN}PASSED!${NC}\n\n"
  else
    printf "${WHITE}Test${NC}:${LRED}FAILED!${NC}\n\n"
fi




#
#	test 2 : from openessl to crypten
#
printf "${BLUE}[+]${NC} Encrypt using openssl\n"

openssl enc -e -in $file -aes-256-cbc -md sha256 -base64 -salt -pass pass:$passwd > enc.out

printf "${GREEN}[+]${NC} Decrypt using crypten.js\n"

$slimer $script d $passwd enc.out > dec.out

res=$(diff dec.out $file)

if [[ -z "$res" ]];
  then
    printf "${WHITE}Test${NC}:${LGREEN}PASSED!${NC}\n\n"
  else
    printf "${WHITE}Test${NC}:${LRED}FAILED!${NC}\n\n"
fi





#
#	test 3 : from crypten to crypten
#
printf "${GREEN}[+]${NC} Encrypt using crypten.js\n"

$slimer $script e $passwd $file >  enc.out
printf "${GREEN}[+]${NC} Decrypt using crypten.js\n"

$slimer $script d $passwd enc.out > dec.out

res=$(diff dec.out $file)

if [[ -z "$res" ]];
  then
    printf "${WHITE}Test${NC}:${LGREEN}PASSED!${NC}\n\n"
  else
    printf "${WHITE}Test${NC}:${LRED}FAILED!${NC}\n\n"
fi

rm enc.out dec.out
