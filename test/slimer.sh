#!/bin/bash
#
#	run tests with slimer 
#
# ------------------------------------
# openssl compatible commands :
# echo <base64 encrypted>      | openssl enc -d -aes-256-cbc -md sha256 -base64 -salt -pass pass:secret
# echo <plain text to encrypt> | openssl enc -e -aes-256-cbc -md sha256 -base64 -salt -pass pass:secret
# ------------------------------------
slimerpath="`pwd`/slimerjs-0.9.6"

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
LRED='\033[1;31m'
LBLUE='\033[1;34m'
LGREEN='\033[1;32m'
WHITE='\033[1;37m'
NC='\033[0m'

Xvfb :1337 &
xpid=$!
export DISPLAY=:1337

$slimerpath/slimerjs "$@"

kill -9 $xpid 2>/dev/null
