#!/bin/bash
#
#   shell client for cryptenbin
#

USAGE="Please specify argument!\n\t-n\tNEW\n\t-r\tREAD\n\t-e\tEDIT"


# decrypt gets a file and passsword as arguments
# e.g. decrypt /etc/passwd secret
# 
function encrypt {
        enc=$(openssl enc -e -in $1 -aes-256-cbc -md sha256 -base64 -salt -pass pass:$2 | tr '\n' ' ' )
        enc=$(echo $enc |sed 's/ //g') # remove whitespaces
        echo $enc
}

# decrypt gets a file and passsword as arguments
# e.g. decrypt /etc/passwd secret
# 
function decrypt {
        echo $(openssl dec -e -in $1 -aes-256-cbc -md sha256 -base64 -salt -pass pass:$2 )
}


if [ $# -eq 0 ]
then
    echo "[!] No arguments supplied"
    echo -e $USAGE
    exit 1
else
    if [[ "$1" == '-n' ]]
    then
        echo '[+] Create new record'
        
        echo "[+] Please specify a file you want to encrypt and press [ENTER]" 
        read FILE
        if [ -f $FILE ];
        then
           echo ""
        else
           echo "File $FILE does not exist."
           exit 1
        fi


        echo "[+] Please write the password to decrypt and press [ENTER]" 
        read passwd
        

    elif [[ "$1" == '-r' ]]
    then
        echo '[+] Read a record'

        echo "[+] Please paste the link you want to read and press [ENTER]" 
        read link
        # __TODO__
        #
        # read content with curl 
        #
        echo ''
        
        echo "[+] Please write the password to decrypt and press [ENTER]" 
        read passwd
        
    elif [[ "$1" == '-e' ]]
    then
        echo '[+] Edit a record'
        
        echo "[+] Please paste the link you want to edit and press [ENTER]"
        read link
        # __TODO__
        #
        # read content with curl 
        #
        echo ''

        #
        # print content
        #
        
        echo "[+] Please specify a file you want to encrypt and press [ENTER]" 
        read FILE
        if [ -f $FILE ];
        then
           echo ""
        else
           echo "File $FILE does not exist."
           exit 1
        fi

        echo "[+] Please write the password to decrypt and press [ENTER]" 
        read passwd

    else
        echo '[!] Unknown argument!'
    fi
fi

echo $loc, $link, $passwd

exit 1

read year



if [[ "$#" == "2" ]]
  then
    passwd="$1"
    file="$2"
  else
    echo 'Please provide password and file';
    echo "   e.g.: $0 '<password>' '<path to file>'";
    exit
fi

enc=$(openssl enc -e -in $file -aes-256-cbc -md sha256 -base64 -salt -pass pass:$passwd | tr '\n' ' ' )
enc=$(echo $enc |sed 's/ //g')

res=$( curl -X POST \
-H 'Accept: application/json, text/javascript, */*;' \
-H 'Content-Type: application/json; charset=UTF-8' \
-H 'User-Agent: BashBashBus' \
-H 'X-Requested-With: XMLHttpRequest' \
--data '{"content":"'$enc'","expiration_code":1}' http://127.0.0.1:8888/save 2>/dev/null )

python -c "import json;r=json.loads('$res');print r.get('write_url'),'\n',r.get('read_url')"
