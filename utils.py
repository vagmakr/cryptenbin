import uuid
import re
import base64
from calendar import monthrange
from datetime import datetime, timedelta
from time import mktime

from os import path, getcwd
import sys

import ujson as json # import json https://news.ycombinator.com/item?id=9326499
from tinydb import TinyDB, Query

ALFANUM = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
dbfile  = 'cryptenbin.json'
dbtable = 'pasten'
title   = "Cryptenbin"
# header  = "Welcome to Cryptenbin"
footer  = ""
script  = {
    'read':'<script src="/static/read.js"></script> ',
    'save':'<script src="/static/save.js"></script> ',
    'edit':'<script src="/static/edit.js"></script> '
}
html    = {
    'read':'''
        <fieldset class="text_fieldset">
            <textarea id="content" name="textarea" 
                    style="border:1px solid #ddd;" readonly></textarea>
        </fieldset>
        <fieldset>
            <input type="text" id="passwd" value='password'/>
            <input id="decrypt" type="submit" value="Decrypt" />
        </fieldset>
    ''',
    'save':'''
        <fieldset class="text_fieldset">
            <!-- Not yet supported <input type="file" id="browse" /> -->
            <textarea id="content" name="textarea">Type your text here</textarea>
        </fieldset>
        <fieldset>
            <input type="text" id="passwd" value='password'/>
            <input id="encrypt" type="submit" value="Encrypt"/>
            <input id="generate" value="Generate" type="submit"
                onClick="$('#passwd').val(randomString(32,'|{}[]$%^*()_+-=;#'))"/>
        </fieldset>
        <fieldset>
            <div class='expires'>
                Delete After
                <select id="expires">
                    <option value=1>1 request</option>
                    <option value=2 selected >1 hour</option>
                    <option value=3>1 day</option>
                    <option value=4>1 week</option>
                    <option value=5>1 month</option>
                    <option value=5>1 year</option>
                    <option value=5>100 years</option>
                </select>
            </div>
        </fieldset>
        <fieldset>
            <input id="save" type="submit" value="Save" />
        </fieldset>
    </div>
    <div class="result" id="result">
        <div class="result" id="read_url">
            <span>Read only:</span>
        </div> 
        <div class="result" id="write_url">
            <span>Edit:</span>
        </div>
        
    ''',
    'edit':'''
        <fieldset class="text_fieldset">
            <textarea id="content" name="textarea">Type your text here</textarea>
        </fieldset>
        <fieldset>
            <input type="text" id="passwd" value='password'/>
            <input id="decrypt" type="submit" value="Decrypt"/>
        </fieldset>
        <fieldset>
            <input id="save" type="submit" value="Save" />
        </fieldset> 
    '''
}

def alfanum(length):
    """ returns random alfanumeric string with given length """
    return ''.join(random.choice(ALFANUM) for i in range(length))

def hash32():
    """ return random hash """
    return uuid.uuid4().hex

def isBase64(s):
    if s:
        try:
            base64.b64decode(s)
            return True
        except:
            return False
    return False

def make_readurl(name):
    return '/read/' + name

def make_writeurl(name, edit_secret):
    return '/edit/' + name + '#' + edit_secret

def validAlfanum(s):
    """ returns TRUE if sting contains alfanumeric characters """
    if sys.version_info.major > 2:
        s = s if s and isinstance(s, str) else ''
    else:
        s = s if s and isinstance(s, str) or isinstance(s, unicode) else ''
    s = True if s and len([i for i in s if i in ALFANUM])==len(s) else False
    return s

def validAlfanumLength(s, length=None):
    """ returns TRUE if sting contains alfanumeric characters
                    and strings lenght matches given length """
    return True if validAlfanum(s) and length == len(s) else False

def get_unique(column, db):
    Found = True
    while Found:
        h = hash32()
        if column == 'name':
            Found = db.search(Query().name == h)
        elif column == 'edit_secret':
            Found = db.search(Query().edit_secret == h)            
        else:
            Found = False
    return h

def notExpired(s=None):
    """
        description:
            check if current time (datetime.now()) is bigger than given timestamp 
        expects: s : timestamp (float)
        returns: True or False 
    """
    return True if s and isinstance(s, float) and datetime.now() < datetime.fromtimestamp(s) else False
    

def get_dates_from_exp_code(code, date_created=None):
    """
        description :
            adds 1h,1d,1m,1y to given date or none
                    <option value=1 selected>1 request</option>
                    <option value=2>1 hour</option>
                    <option value=3>1 day</option>
                    <option value=4>1 week</option>
                    <option value=5>1 month</option>
                    <option value=6>1 year</option>
                    <option value=7>100 years</option>
        expects: code : int 1 to 5
        returns: timestamp or None
    """
    code = code if code and isinstance(code, int) and code in range(1,8) else None
    dt = date_created if isinstance(date_created, datetime) else datetime.now()
    if   code==1: # for 1 request
        exp_dt = None 
    elif code==2: # add a hour
        exp_dt = dt+timedelta(hours=1)
    elif code==3: # add a day
        exp_dt = dt+timedelta(days=1)
    elif code==4: # add a week
        exp_dt = dt+timedelta(days=7)
    elif code==5: # add a month
        new_year=dt.year+(dt.month/12)
        newmonth=(dt.month % 12) + 1
        days_in_month = calendar.monthrange(newyear, newmonth)[1]
        newday = min(dt.day, days_in_month)
        exp_dt = datetime(new_year, newmonth, newday,
                        dt.hour, dt.minute, dt.second)
    elif code==6: # add a year
        exp_dt = datetime(dt.year+1, dt.month, dt.day,
                        dt.hour, dt.minute, dt.second)
    elif code==7: # 100 years are enough -- almost never
        exp_dt = datetime(dt.year+100, dt.month, dt.day,
                        dt.hour, dt.minute, dt.second)
    else:
        exp_dt=None
        dt = None
    
    return mktime(dt.timetuple()) if isinstance(dt, datetime) else None, \
            mktime(exp_dt.timetuple()) if isinstance(exp_dt, datetime) else None
