#!/usr/bin/env python
'''

    CRYPTENBIN
    -----------------------------------------------------------------------

        __TODO__ :
    [-] custom 404 error for static pages
    [-] Add bar for links like about (index.html)
    [-] create about page
    [-] hightlight area to follow up
    

'''
import tornado.auth
import tornado.escape
import tornado.httpserver
import tornado.ioloop
import tornado.web
from tornado.options import define, options
from utils import *

define("port", default=8888, help="run on the given port", type=int)
db = TinyDB(dbfile)
pasten = db.table(dbtable)

class My404Handler(tornado.web.RequestHandler):
    # Override prepare() instead of get() to cover all possible HTTP methods.
    def prepare(self):
        self.clear()
        self.set_status(404)
        self.finish("<html><body><h1>"+str(404)+"</h1></body></html>")

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/",                    MainHandler     ),
            (r"/read/([a-zA-Z0-9]+)", ReadHandler,    ), # dict(db=pasten)),
            (r"/raw/([a-zA-Z0-9]+)",  RawHandler,     dict(db=pasten)),
            (r"/edit/([a-zA-Z0-9]+)", EditHandler,    dict(db=pasten)),
            (r"/save",                SaveHandler,    dict(db=pasten)),
            (r"/save/([a-zA-Z0-9]+)", UpdateHandler,  dict(db=pasten)),
            (r"/about",               AboutHandler,  )
        ]

        settings = dict(
            template_path=path.join(path.dirname(__file__), "templates"),
            static_path=path.join(path.dirname(__file__), "static"),
            debug=True,
            autoescape=None,
            default_handler_class=My404Handler
        )
        tornado.web.Application.__init__(self, handlers, **settings)


class AboutHandler(tornado.web.RequestHandler):
    '''     MAIN    '''
    def get(self):
        self.render( "index.html",
            page_title  = "About",
            script      = "",
            content     = """
            <center style="margin-top:9em;">
            Just another crypto service.
            contact vagmakr[at]openmailbox.org for more info.
            </center>
            """,
            footer_text = '')

class MainHandler(tornado.web.RequestHandler):
    '''     MAIN    '''
    def get(self):
        # set No-Cache
        self.set_header('Last-Modified', datetime.now().isoformat())
        self.set_header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0')
        self.set_header('Pragma','no-cache')
        self.set_header('Expires', '-1')

        self.render( "index.html",
            page_title  = title,
            script      = script['save'],
            # header_text = header,
            content     = html['save'],
            footer_text = footer
        )

class ReadHandler(tornado.web.RequestHandler):
    """     READ    """
    # def initialize(self, db): self.db = db
    
    def get(self, name=None):
        self.render( "index.html",
            page_title  = title,
            script      = script['read'],
            # header_text = header,
            content     = html['read'],
            footer_text = footer
        )


class EditHandler(tornado.web.RequestHandler):
    """     Edit    """
    def initialize(self, db):
        self.db = db

    def get(self, name=None):
        self.render( "index.html",
            page_title  = name,
            script      = script['edit'],
            # header_text = header,
            content     = html['edit'],
            footer_text = footer
        )

class RawHandler(tornado.web.RequestHandler):
    """     RAW    """
    def initialize(self, db):
        self.db = db
        self.update = False

    def write_error(self, status_code, **kwargs):
        self.clear()
        self.set_status(status_code)
        self.finish(str(status_code)+" Error!")

    def on_finish(self):
        if self.update:
            self.db.clear_cache()
            self.db.write(self.db.all())

    def get(self, name=None):
        res = self.db.search(Query().name == name)
        if res:
            if notExpired(res[0].get('date_expired')):   # check if expired
                self.set_header("Content-Type", "text/plain")
                self.finish(res[0].get('content'))
                return
            else:                                        # remove if expired
                if not res[0].get('date_expired') and res[0].get('exp_code') == 1:
                    self.set_header("Content-Type", "text/plain")
                    self.finish(res[0].get('content'))
                    self.db.remove(Query().name == name)
                    self.update = False
                    return
                else:
                    self.db.remove(Query().name == name)
                    self.update = False
        # self.write_error(404)
        raise tornado.web.HTTPError(404)
    
    def post(self, name=None):
        data = json.loads(self.request.body) if self.request.body else None 
        if not data:
            self.finish({"status":"error","msg":"Not recognized request! Expected ajax post request"})
            return
        
        if not validAlfanumLength(name, 32):
            self.finish({"status":"error","msg":"Expected alfanumeric 'name' 32 characters long"})
            return

        res = self.db.search(Query().name == name)
        if res:
            if notExpired(res[0].get('date_expired')):  # check if expired
                self.finish({'status':'success','content':res[0].get('content')})
                return

            else:                                        # remove if expired
                if not res[0].get('date_expired') and res[0].get('exp_code') == 1:
                    self.set_header("Content-Type", "text/plain")
                    self.finish({'status':'success','content':res[0].get('content')})
                    self.db.remove(Query().name == name)
                    self.update = False
                    return
                else:
                    self.db.remove(Query().name == name)
                    self.update = False
                    self.finish({"status":"error","msg":"Expired!"})
                    return
        else:           
            self.finish({'status':'error','msg':'Not Found!'})

class SaveHandler(tornado.web.RequestHandler):
    def initialize(self, db):
        self.db = db
        self.update = False
    
    def on_finish(self):
        if self.update:
            self.db.clear_cache()
            self.db.write(self.db.all())

    def post(self):
        data   = json.loads(self.request.body) if self.request.body else None 
        rtrn   = {}
        status = {'status':'success'}
        if not data:
            self.finish({"status":"error","msg":"Not recognized request! Expected ajax post request"})
            return

        # validate content
        rtrn['content'] = data.get('content').strip() if data.get('content') else ''
        if not isBase64(rtrn['content']): # if content is not defined or is not in base64 format
            self.finish({"status":"error","msg":"Expected content in base64 format"})
            return

        # validate encryption_type
        rtrn['enc_type'] = data.get('encryption_type').strip() if data.get('encryption_type')  else ''
        rtrn['enc_type'] = rtrn['enc_type'] if validAlfanumLength(rtrn['enc_type'], 10) else ''
        # if not validAlfanumLength(rtrn['enc_type'], 10):
        #     rtrn['enc_type'] = ''
        #     status={
        #         'status':'warn', 
        #         "msg":""" 'encryption_type' should be an alfanumeric string less than 32,
        #             Record saved with empty encryption type"""}
        
        # get expiration date
        rtrn['exp_code'] = int(data['expiration_code']) if data.get('expiration_code') else None
        rtrn['date_created'], rtrn['date_expired'] = get_dates_from_exp_code(rtrn['exp_code'])
        if not rtrn['date_created'] and not rtrn['date_expired']:
            self.finish({"status":"error","msg":"'expiration_code' should be set and be an intreger from 1 to 5"})
            return
        
        rtrn['name']        = get_unique('name', self.db)
        rtrn['edit_secret'] = get_unique('edit_secret', self.db)
        rtrn['read_url']    = make_readurl(rtrn['name'])
        rtrn['write_url']   = make_writeurl(rtrn['name'],rtrn['edit_secret'])
        self.db.insert(rtrn)
        self.update = False
        del rtrn['content']
        rtrn.update(status)
        self.finish(rtrn)

class UpdateHandler(tornado.web.RequestHandler):
    """
        Endpoint accesible from javascript / bash client, not visible to the user
    """
    def initialize(self, db):
        self.db = db
        self.update = False

    def on_finish(self):
        if self.update:
            self.db.clear_cache()
            self.db.write(self.db.all())

    def post(self, name=None):
        data = json.loads(self.request.body) if self.request.body else None 
        rtrn = {}
        status = {'status':'success'}
        if not data:
            self.finish({"status":"error","msg":"Not recognized request! Expected ajax post request"})
            return

        # validate content
        rtrn['content'] = data.get('content').strip() if data.get('content') else ''
        if not isBase64(rtrn['content']): # if content is not defined or is not in base64 format
            self.finish({"status":"error","msg":"Expected content in base64 format"})
            return

        # validate encryption_type
        rtrn['enc_type'] = data.get('encryption_type').strip() if data.get('encryption_type')  else ''
        rtrn['enc_type'] = rtrn['enc_type'] if validAlfanumLength(rtrn['enc_type'], 10) else ''

        # validate name
        if not validAlfanumLength(name, 32):
            self.finish({"status":"error","msg":"Expected alfanumeric 'name' 32 characters long"})
            return

        # get entry by name
        res = self.db.search(Query().name == name)
        if not res:
            self.finish({"status":"error","msg":"Not Found!"})
            return
        
        # check if expired
        if notExpired(res[0].get('date_expired')):

            # validate edit_secret
            edit_secret = data.get('edit_secret') if data.get('edit_secret') else ''
            if not res[0].get('edit_secret') == edit_secret:
                self.finish({"status":"error","msg":"Secret for editing doesn't match"})
                return
            
            self.db.update({'content': rtrn.get('content')}, Query().name == name)
            self.updated = True
            rtrn.update(status)
            self.finish(rtrn)

        # remove if expired
        else:
            self.db.remove(Query().name == name)
            self.finish({"status":"error","msg":"Expired!"})
            return

def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port, address='127.0.0.1')
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted! bye bye')
        db.close()
        exit(0)
